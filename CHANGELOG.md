[0.1.0]
* Initial version

[0.2.0]
* Add Qt and Cloudron OAuth providers

[0.3.0]
* Use Mattermost 2.1.0
* Remove Qt and Cloudron OAuth providers

[0.4.0]
* Update to Mattermost 2.2.0

[0.4.1]
* Removed unneeded OAuth proxy

[0.4.2]
* Use new sendmail credentials

[0.5.0]
* Update to Mattermost 3.2.0
* The first team you created will get chosen as the 'Primary Team' for the sake
  of merging duplicate email ids.

[0.5.1]
* Fix icon

[0.5.2]
* Add postInstallMessage

[0.6.0]
* Mattermost 3.4.0

[0.7.0]
* Mattermost 3.5.1

[0.7.1]
* Fix description

[0.7.2]
* Enalbe log viewing

[0.8.0]
* Update to Mattermost 3.6.2
* Update to base image 0.10.0

[0.9.0]
* Update to Mattermost 3.7.3

[0.10.0]
* Update to Mattermost 3.8.2

[0.11.0]
* Update to Mattermost 3.9.0

[0.11.1]
* Update description

[0.12.0]
* Update to Mattermost 3.10.0

[0.13.0]
* Update to Mattermost 4.0.0

[0.13.1]
* Update to Mattermost 4.0.1

[1.0.0]
* Update to Mattermost 4.0.2

[1.0.1]
* Update to Mattermost 4.0.3

[1.0.2]
* tail the logs
* Fix issue where iOS and Android app always showed 'connecting...'

[1.1.0]
* Update to Mattermost 4.1.0
* Includes unannounced security updates (https://about.mattermost.com/security-updates/)

[1.2.0]
* Update to Mattermost 4.2.0

[1.2.1]
* Allow CORS requests from all origins to make the iOS app work

[1.3.0]
* Update to Mattermost 4.3.0

[1.4.0]
* Use TLS for relaying email

[1.4.1]
* Update to Mattermost 4.3.1

[1.4.2]
* Update to Mattermost 4.3.2

[1.5.0]
* Update Mattermost to 4.4.1
* https://docs.mattermost.com/administration/changelog.html#release-v4-4-1
* Do Not Disturb Status
* Added emoji picker to the Edit Message dialog.
* Added /remove and /kick slash commands to remove a user from the channel.
* When you have multiple browser tabs open and receive a video call, the
  ringtone stops in all tabs when you accept the call.

[1.5.1]
* Update Mattermost to 4.4.2

[1.6.0]
* Fix issues where existing config.json is not updated with (new) config variables from a new version

[1.6.1]
* Update Mattermost to 4.4.3

[1.6.2]
* Update Mattermost to 4.4.4

[1.6.3]
* Update Mattermost to 4.4.5

[1.7.0]
* Update Mattermost to 4.5.0

[1.7.1]
* Update Mattermost to 4.5.1

[1.8.0]
* Update Mattermost to 4.6.1

[1.8.1]
* Update Mattermost to 4.6.2

[1.9.0]
* Update Mattermost to 4.7.1
* [Release post](https://about.mattermost.com/releases/mattermost-4-7/)
* Enhanced image previews and thumbnails
* Faster Load Times
* Introducing Desktop App 4.0
* ‘Unreads’ Sidebar Setting

[1.9.1]
* Update Mattermost to 4.7.2

[1.9.2]
* Update Mattermost to 4.7.3

[1.10.0]
* Update Mattermost to 4.8
* [Release post](https://about.mattermost.com/releases/mattermost-4-8/)

[1.10.1]
* Update Mattermost to 4.8.1

[1.11.0]
* Update Mattermost to 4.9.0
* [Release post](https://about.mattermost.com/releases/mattermost-4-9/)
* Reclaim time and enjoy fewer distractions with muted channels
* Display the names of colleagues whichever way helps you find and recognize them fastest by adjusting the teammate name display setting
* Customize your employees’ Mattermost display with team icons so users can toggle between teams more easily
* Increase compliance and archive your data with automatic exporting to Global Relay in Enterprise E20 as a premium governance add-on
* Our mobile app is even more user-friendly thanks to several performance improvements added to our new release

[1.11.1]
* Update Mattermost to 4.9.1

[1.11.2]
* Update Mattermost to 4.9.2

[1.11.3]
* Update Mattermost to 4.9.3

[1.12.0]
* Update Mattermost to 4.10.0
* [Release post](https://about.mattermost.com/releases/mattermost-4-10/)
* Faster load times – enjoy a smoother experience thanks to significantly faster loading times during start up and after refresh.
* Convert channels to private – keep important discussions confidential by easily converting public channels into private channels.
* Cleaner mobile experience – Mattermost Mobile App 1.8 is packed with a number of performance improvements designed to increase team productivity.

[1.12.1]
* Update Mattermost to 4.10.1

[1.13.0]
* Update Mattermost to 5.0.0
* [Release post](https://about.mattermost.com/releases/mattermost-5-0/)
* Intercept and modify posts - customize Mattermost by creating plugins to automatically restrict, modify, censor and link posts
* Advanced permissions - ensure everyone has the proper level of access with new system and team permissions schemes
* Longer posts - increase productivity with significantly longer messages and better formatting
* Combined join/leave messages - reduce clutter with combined system messages whenever someone joins or leaves a channel
* APIv4 replaces APIv3 - migrate from APIv3 to APIv4 if you haven’t done so already, as APIv3 is no longer available with the arrival of Mattermost 5.0

[1.13.1]
* Update Mattermost to 5.0.2

[1.14.0]
* Update Mattermost to 5.1.0

[1.14.1]
* Update Mattermost to 5.1.1

[1.15.0]
* Update Mattermost to 5.2.1

[1.15.1]
* Update Mattermost to 5.2.2

[1.16.0]
* Update Mattermost to 5.3.1

[1.17.0]
* Use latest base image

[1.17.1]
* Use the mattermost binary

[1.18.0]
* Update Mattermost to 5.4.0

[1.18.1]
* Fix issue where mattermost errors when plugins are enabled

[1.19.0]
* Update Mattermost to 5.5.0

[1.20.0]
* Update Mattermost to 5.6.0

[1.20.1]
* Update Mattermost to 5.6.2

[1.20.2]
* Update Mattermost to 5.6.3

[1.21.0]
* Update Mattermost to 5.7.0

[1.21.1]
* Update Mattermost to 5.7.1

[1.21.2]
* Update Mattermost to 5.7.2

[1.22.0]
* Update Mattermost to 5.8.0

[1.22.1]
* Update Mattermost to 5.8.1

[1.23.0]
* Update Mattermost to 5.9.0

[1.24.0]
* Update Mattermost to 5.10.0

[1.24.1]
* Update Mattermost to 5.10.1

[1.25.0]
* Update Mattermost to 5.11.0

[1.26.0]
* Update manifest to v2
* Update Mattermost to 5.12.0

[1.26.1]
* Update Mattermost to 5.12.1

[1.26.2]
* Update Mattermost to 5.12.2

[1.26.3]
* Update Mattermost to 5.12.3

[1.26.4]
* Update Mattermost to 5.12.4

[1.27.0]
* Update Mattermost to 5.13.0

[1.27.1]
* Update Mattermost to 5.13.1

[1.27.2]
* Update Mattermost to 5.13.2

[1.28.0]
* Update Mattermost to 5.14.0

[1.28.1]
* Update Mattermost to 5.14.1

[1.28.2]
* Update Mattermost to 5.14.2

[1.28.3]
* Update Mattermost to 5.14.3

[1.29.0]
* Update Mattermost to 5.15.0

[1.29.1]
* Update Mattermost to 5.15.1

[1.30.0]
* Update Mattermost to 5.16.0

[1.30.1]
* Update Mattermost to 5.16.1

[1.30.2]
* Update Mattermost to 5.16.2

[1.30.3]
* Update Mattermost to 5.16.3

[1.31.0]
* Update Mattermost to 5.17.0

[1.31.1]
* Update Mattermost to 5.17.1

[1.32.0]
* Update Mattermost to 5.18.0

[1.32.1]
* Update Mattermost to 5.18.1

[1.32.2]
* Update Mattermost to 5.18.2

[1.33.0]
* Update Mattermost to 5.19.0

[1.33.1]
* Update Mattermost to 5.19.1

[1.34.0]
* Update Mattermost to 5.20.1

[1.34.1]
* Update Mattermost to 5.20.2

[1.35.0]
* Update Mattermost to 5.22.0

[1.36.0]
* Update Mattermost to 5.22.1

[1.36.1]
* Update Mattermost to 5.22.2

[1.36.2]
* Update Mattermost to 5.22.3

[1.37.0]
* Update Mattermost to 5.23.0

[1.37.1]
* Update Mattermost to 5.23.1

[1.38.0]
* Update Mattermost to 5.24.0
* Enable link previews by default

[1.39.0]
* Update Mattermost to 5.25.0
* Update screenshots and forumUrl

[1.39.1]
* Update Mattermost to 5.25.1
* [Changelog](https://github.com/mattermost/mattermost-server/releases/tag/v5.25.1)

[1.39.2]
* Update Mattermost to 5.25.3
* [Changelog](https://github.com/mattermost/mattermost-server/releases/tag/v5.25.3)

[1.40.0]
* Update Mattermost to 5.26.0
* [Changelog](https://github.com/mattermost/mattermost-server/releases/tag/v5.26.0)

[1.40.1]
* Update Mattermost to 5.26.1
* [Changelog](https://github.com/mattermost/mattermost-server/releases/tag/v5.26.1)

[1.40.2]
* Update Mattermost to 5.26.2
* [Changelog](https://github.com/mattermost/mattermost-server/releases/tag/v5.26.2)

[1.41.0]
* Update Mattermost to 5.27.0
* [Changelog](https://github.com/mattermost/mattermost-server/releases/tag/v5.27.0)

[1.42.0]
* Update Mattermost to 5.28.0
* [Changelog](https://github.com/mattermost/mattermost-server/releases/tag/v5.28.0)

[1.42.1]
* Update Mattermost to 5.28.1
* [Changelog](https://github.com/mattermost/mattermost-server/releases/tag/v5.28.1)

[1.42.2]
* Bring config.json.template up to speed with latest config.json

[1.43.0]
* Update Mattermost to 5.29.0
* [Changelog](https://github.com/mattermost/mattermost-server/releases/tag/v5.29.0)

[1.43.1]
* Update Mattermost to 5.29.1
* [Changelog](https://github.com/mattermost/mattermost-server/releases/tag/v5.29.1)

[1.44.0]
* Update Mattermost to 5.30.1
* [Changelog](https://github.com/mattermost/mattermost-server/releases/tag/v5.30.1)

[1.45.0]
* Update Mattermost to 5.31.0
* [Changelog](https://github.com/mattermost/mattermost-server/releases/tag/v5.31.0)

[1.45.1]
* Update Mattermost to 5.31.1
* Update base image to version 3
* Fixed an issue where the config.json was sporadically getting reset upon CLI command execution. MM-32234
* Fixed an issue where FeatureFlags section was getting erroneously written to config.json. MM-32389
* Fixed an issue where channels were sometimes removed from custom categories when a user left a team. MM-30314
* Fixed an issue where users were unable to mark Direct Messages in a thread as unread. MM-32253
* Fixed an issue where PermanentDeleteChannel failed with “failed to get a thread” error. MM-31731
* [Changelog](https://docs.mattermost.com/administration/changelog.html#release-v5-31-esr)

[1.46.0]
* Update Mattermost to 5.32.0
* [Changelog](https://docs.mattermost.com/administration/changelog.html#release-v5-32-feature-release)

[1.46.1]
* Update Mattermost to 5.32.1

[1.47.0]
* Update Mattermost to 5.33.0

[1.47.1]
* Update Mattermost to 5.33.1

[1.47.2]
* Update Mattermost to 5.33.2

[1.47.3]
* Update Mattermost to 5.33.3

[1.48.0]
* Update Mattermost to 5.34.2

[1.49.0]
* Update Mattermost to 5.35.1

[1.49.1]
* Update Mattermost to 5.35.2

[1.49.2]
* Update Mattermost to 5.35.3

[1.50.0]
* Update Mattermost to 5.36.0

[1.50.1]
* Update Mattermost to 5.36.1

[1.51.0]
* Update Mattermost to 5.37.0

[1.51.1]
* Update Mattermost to 5.37.1

[1.52.0]
* Update Mattermost to 5.38.0

[1.52.1]
* Update Mattermost to 5.38.1

[1.52.2]
* Update Mattermost to 5.38.2

[1.53.0]
* Update Mattermost to 5.39.0

[1.54.0]
* Update Mattermost to 6.0.0

[1.54.1]
* Update Mattermost to 6.0.1

[1.54.2]
* Update Mattermost to 6.0.2

[1.55.0]
* Update Mattermost to 6.1.0

[1.56.0]
* Update Mattermost to 6.2.1

[1.57.0]
* Update Mattermost to 6.3.0

[1.57.1]
* Do not overwrite reply-to and feedback name

[1.57.2]
* Update Mattermost to 6.3.1

[1.57.3]
* Update Mattermost to 6.3.2

[1.57.4]
* Update Mattermost to 6.3.3

[1.58.0]
* Make templates editable

[1.59.0]
* Update Mattermost to 6.4.0

[1.59.1]
* Update Mattermost to 6.4.1

[1.60.0]
* Update Mattermost to 6.4.0
* Fix database migration issue with previous package

[1.60.1]
* Update Mattermost to 6.4.2

[1.61.0]
* Update Mattermost to 6.5.0

[1.62.0]
* Update Mattermost to 6.6.0

[1.62.1]
* Update Mattermost to 6.6.1
* [Full changelog](https://github.com/mattermost/mattermost-server/releases/tag/v6.6.1)
* medium level security fix

[1.63.0]
* Update Mattermost to 6.7.0
* [Full changelog](https://github.com/mattermost/mattermost-server/releases/tag/v6.7.0)

[1.63.1]
* Update Mattermost to 6.7.1
* [Full changelog](https://github.com/mattermost/mattermost-server/releases/tag/v6.7.1)
* Medium level security fixes

[1.64.0]
* Email display name support

[1.64.1]
* Update Mattermost to 6.7.2
* [Full changelog](https://github.com/mattermost/mattermost-server/releases/tag/v6.7.2)

[1.65.0]
* Update Mattermost to 7.0.1
* [Full changelog](https://github.com/mattermost/mattermost-server/releases/tag/v7.0.0)
* Mattermost Platform Release v7.0.0 includes the general availability of collapsed reply threads, voice calls and screen share (beta), messaging format toolbar, Playbooks inline editor, usage statistics, and ability to change triggers and actions mid-run.

[1.66.0]
* Update Mattermost to 7.1.0
* [Full changelog](https://github.com/mattermost/mattermost-server/releases/tag/v7.1.0)

[1.66.1]
* Update Mattermost to 7.1.1
* [Full changelog](https://github.com/mattermost/mattermost-server/releases/tag/v7.1.1)

[1.66.2]
* Update Mattermost to 7.1.2
* [Full changelog](https://github.com/mattermost/mattermost-server/releases/tag/v7.1.2)

[1.67.0]
* Update Mattermost to 7.2.0
* [Full changelog](https://github.com/mattermost/mattermost-server/releases/tag/v7.2.0)

[1.68.0]
* Update Mattermost to 7.3.0
* [Full changelog](https://github.com/mattermost/mattermost-server/releases/tag/v7.3.0)

[1.69.0]
* Update Mattermost to 7.4.0
* [Full changelog](https://github.com/mattermost/mattermost-server/releases/tag/v7.4.0)

[1.69.1]
* Fix issue where email display name cannot have single quotes

[1.70.0]
* Update Mattermost to 7.5.0
* [Full changelog](https://github.com/mattermost/mattermost-server/releases/tag/v7.5.0)

[1.70.1]
* Update Mattermost to 7.5.1
* [Full changelog](https://github.com/mattermost/mattermost-server/releases/tag/v7.5.1)

[1.70.2]
* Update Mattermost to 7.5.2
* [Full changelog](https://github.com/mattermost/mattermost-server/releases/tag/v7.5.2)

[1.71.0]
* Update Mattermost to 7.7.0
* [Full changelog](https://github.com/mattermost/mattermost-server/releases/tag/v7.7.0)

[1.71.1]
* Update Mattermost to 7.7.1
* [Full changelog](https://github.com/mattermost/mattermost-server/releases/tag/v7.7.1)

[1.71.2]
* Add UDP port for calls plugin

[1.72.0]
* Update Mattermost to 7.8.0
* [Full changelog](https://github.com/mattermost/mattermost-server/releases/tag/v7.8.0)

[1.72.1]
* Update Mattermost to 7.8.1
* [Full changelog](https://github.com/mattermost/mattermost-server/releases/tag/v7.8.1)

[1.73.0]
* Update Mattermost to 7.9.0
* [Full changelog](https://github.com/mattermost/mattermost-server/releases/tag/v7.9.0)

