FROM cloudron/base:4.0.0@sha256:31b195ed0662bdb06a6e8a5ddbedb6f191ce92e8bee04c03fb02dd4e9d0286df

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

ARG VERSION=7.9.0
RUN curl -L https://releases.mattermost.com/${VERSION}/mattermost-${VERSION}-linux-amd64.tar.gz | tar -zxf - --strip-components=1 -C /app/code
RUN npm install json

RUN ln -s /app/data/plugins /app/code/plugins && \
    ln -s /app/data/client/plugins /app/code/client/plugins && \
    mv /app/code/templates /app/code/templates.original && \
    ln -s /app/data/templates /app/code/templates

RUN mkdir -p /home/cloudron/.config && \
    ln -s /app/data/mmctl /home/cloudron/.config/mmctl

COPY templates.README json-merge.js config.json.template start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
